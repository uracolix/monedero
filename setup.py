#
# run with python setup.py build_ext -i
#
#from distutils.core import setup, Extension
import setuptools

from monedero import __version__ as PackageVersion
setuptools.setup(name='monedero',
    version=PackageVersion,
    description='monedoero - A Paper Wallet Generator',
    author='Axel Wachtler',
    author_email='uracolix@quantentunnel.de',
    url='http://uracoli.nongnu.org',
    #packages=setuptools.find_packages(where="."),
    packages = ["monedero", "monedero.img"],
    install_requires=["reportlab", "requests", "cryptos"],
    package_data={'monedero.img' : ['*.png'
                       #'monedero/img/blackcoin-logo.png',
                       #'monedero/img/bch.png',
                       #'monedero/img/btc.png',
                       #'monedero/img/dash.png',
                       #'monedero/img/doge.png',
                       #'monedero/img/ltc.png'                       
                       ]},
    include_package_data=True,
    scripts = ["mk_wallet.py"]
)
