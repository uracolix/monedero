from monedero import RpcHost, CoinParams
from pprint import pprint as pp
try:
    import readline, rlcompleter
    readline.parse_and_bind("tab:complete")
except:
    pass

if __name__ == '__main__':
    rpcHost = "localhost"
    rpcPort = 8332
    rpcUser, rpcPassword = CoinParams.getRpcCredentialsFromConfig()
    rh = RpcHost.RPCHost()
    rh.setServerUrl(rpcHost, rpcPort, rpcUser, rpcPassword)

    max_blocks = rh.call("getblockcount")
    nb_tx = 10000
    stakers = {}
    for blk in xrange(max_blocks-nb_tx, max_blocks):
        block_info = rh.call("getblockbynumber", blk)
        stake_diff = block_info['difficulty']
        stake_time = block_info['time']
        stake_txid = block_info['tx'][1]
        stake_tx = rh.call("gettransaction", stake_txid)
        stake_sum = sum([v.get('value', 0) for v in stake_tx['vout']])
        stake_addr = []
        for v in stake_tx['vout']:
            stake_addr += v['scriptPubKey'].get('addresses',[])

        for a in stake_addr:
            try:
                stakers[a]['cnt'] += 1
            except:
                stakers[a] = {'cnt' : 1}
            stakers[a]['sum'] = stake_sum
        
        #print "%d, %d, %16.8f, %d, %d, %s" % (blk, stake_time, stake_sum, stake_diff, len(stake_addr), ":".join(stake_addr))

    print "Stats on Blocks %d - %d" % (max_blocks-nb_tx, max_blocks)
    print "Blocks: %d" % nb_tx
    print "Number Stakers %d" % len(stakers.keys())
    print "Top-Stakers:"
    for k,v in stakers.items():
        if v['cnt'] > 100:
            print k, v
            
