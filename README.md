# Monedero - Generating Paper Wallets with Python

Storing CryptoCoin Keys in Paper Wallets is a way to backup the secret key and
store it in a safe place. Moenedero generates several CryptoCoin keys and generates a PDF
file that can be printed.

## Usage

	mk_wallet.py -h 
 
Show the help message and exit
 	
 	mk_wallet.py 
 	
Generate a fresh CryptoCoin key in the file `wallet.pdf`

**Example Sessions**

Plain call without parameters

	$ python mk_wallet.py 
	enter pdf password: <strengeheim>
	confirm pdf password: <strengeheim>
	enter coin ['BCH', 'BTC', 'DASH', 'DOGE', 'LTC']: btc
	enter priv key option [1 = enter seed phrase, 2 = enter key]:1
	enter seed phrase:Intuition ist Intelligenz mit überhöhter Geschwindigkeit.
	coin: Bitcoin BTC
	addr: 1EVuNuaBmUjEmhADWyDNRmR3Ttuhr3q3ZL
	Wrote file: wallet.pdf

Show secrets on console (-s), use PDF password (-P test), select coin (-c BCH)

	$ python mk_wallet.py -s -P test -c BCH
	enter priv key option [1 = enter seed phrase, 2 = enter key]:1
	enter seed phrase:Intuition ist Intelligenz mit überhöhter Geschwindigkeit.
	coin: Bitcoin Cash BCH
	addr: 1EVuNuaBmUjEmhADWyDNRmR3Ttuhr3q3ZL
	seed: Intuition ist Intelligenz mit überhöhter Geschwindigkeit.
	priv: d026196d5491e349dbde975c957c9dc5fbf5103927da49654be3a7df07788478
	Wrote file:  wallet.pdf



## Installation

 	pip install -e git+https://gitlab.com/uracolix/monedero.git

## Supported Coins

Currently all coins supported from "cryptos": ['BCH', 'BTC', 'DASH', 'DOGE', 'LTC']


## Create an Executable w/ pyinstaller

	pyinstaller \
		--collect-data cryptos \
		--collect-all reportlab \
		--add-data "monedero/img:monedero/img" \
		--onefile mk_wallet.py

## External SW

  * **cryptos** : Bitcoin related Python module
	https://github.com/primal100/pybitcointools/
  * **reportlab** : PDF generation
    https://docs.reportlab.com/
  * **Logos** : High quality Logos for almost all CryptoCoins
    https://cryptologos.cc/
