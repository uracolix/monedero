.. Monedero documentation master file, created by
   sphinx-quickstart on Sun Dec 31 09:16:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Monedero - Generating Paper Wallets with Python and Reportlab
=============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst

   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
