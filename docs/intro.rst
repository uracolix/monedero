
Introduction
------------

The script `mk_wallet.py` generates a (encrypted) PDF file that displays the private key and 
public *Coin address.

Usage:: 

    mk_wallet.py [options]

Options
-------

For the key generation the script http://we.lovebitco.in/paperwal.py is employed.

Note::
	The author of mondero takes no responsibility for the cryptographic correctnes
	of the generated keys.


Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -e, --encrypted       create BIP38-encrypted privkey (takes a LONG time)
  -v, --validate        enable extensive system tests for entropy
  -s, --silent          disable most console output except address
  -l, --loop            restart instead of exit
  -p, --nopause         disable the pause before exiting
  -d, --doublecalc      calculate twice and test results
  -z JUST A HELPFUL HINT
                        try ctrl-tab to abort the program
  -o PDFFILENAME, --output=PDFFILENAME
                        name of the generated PDF file (default: wallet.pdf)
  -P PDFPASSWD, --password=PDFPASSWD
                        protect PDF file with password

  Warning:
    If you use this option, you should supply REAL randomly generated
    entropy. It is probably a good idea not to reuse a seed.

    -r ENTROPY, --entropy=ENTROPY
                        random seed instead of keypresses, 64+ characters

Examples
---------

./monedero.py
    
    Query for a PDF passphrase and requests entropy key presses
    and generates wallet.pdf
    
./monedero.py -o my_wallet.pdf 

    Write the file my_wallet.pdf
    
./monedero.py -P mysupersecurepasswd

    For Batch Generation of several paper wallets, you can provide 
    the PDF passphrase by command line. !Attention! the passphrase 
    might be stored in the command line history.

See also http://monedero.readthedocs.io for full documentation.

Best Practices
--------------

Here is a collection of ideas how to make the paper-wallet process more
secure 

* Use a Network-disconnected PC.
* Also use a directly connected printer if possible
* After generating a new paper wallet verify if the public wallet address
  is not known already. Use a block chain explorer, e.g.:
  https://chainz.cryptoid.info/blk/

  If wallet is known and there are already coinz on the wallet: 
  Don't use the wallet and the private key, this are the coinz of someone else.
  
* Perform the entire wallet generation on a Ram-Disk::

      sudo mkdir /mnt/ramdisk
      sudo mount -t tmpfs -o size=512m tmpfs /mnt/ramdisk

* Always secure the generated PDF File with a good passphrase.


  
