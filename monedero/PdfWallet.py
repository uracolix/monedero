from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, landscape, letter
from reportlab.lib import pdfencrypt
from reportlab.lib.colors import black, blue, green, red, white
from reportlab.graphics.barcode import qr
from reportlab.graphics.shapes import Drawing
from reportlab.graphics import renderPDF
import textwrap
import time
import sys
import os
import monedero

class PdfWallet:
    def __init__(self, privk = "privk", pubk = "pubk", coin = "unknown", seed="??", fileName = "wallet.pdf", encryptPdf = False, pageSize = A4):
        self.privk = privk
        self.pubk = pubk
        self.coin = coin
        self.seed = seed
        self.fileName = fileName
        self.encryptPdf = encryptPdf
        self.pageSize = pageSize
        self.dataDir = os.path.join(os.path.dirname(os.path.realpath(monedero.__file__)) , "img")

    def render(self):
        if self.encryptPdf and type(self.encryptPdf) == type(""):
            enc = pdfencrypt.StandardEncryption(self.encryptPdf, canPrint=0)
        else:
            enc = None

        c = self.canvas = canvas.Canvas(self.fileName, pagesize = landscape(self.pageSize), encrypt = enc)
        width,height = self.pageSize

        self.renderShader() # empty for now
        self.renderBackground()
        self.renderPrivKey()
        self.renderPubKey()

    def save(self):
        self.canvas.showPage()
        self.canvas.save()

    def renderBackground(self):
        c = self.canvas
        width,height = self.pageSize

        # white sections for keys
        self.privRectangle = (10,10, (height/2)-20, width/4-20)
        self.seedRectangle = (10, 2, 20, 30)
        self.pubRectangle = (10,10+(2*width/4), (height/2)-20,width/4-20)
        c.setFillColor(white)
        c.rect(*self.privRectangle, fill=True, stroke=False)
        c.rect(*self.pubRectangle, fill=True, stroke=False)
        c.rect(*self.seedRectangle, fill=True, stroke=False)
        c.setFillColor(black)

        # folding lines
        c.line(0,width/4, height/2, width/4)
        c.line(0,width*2/4, height/2, width*2/4)
        c.line(0,width*3/4, height/2, width*3/4)
        c.line(height/2,0, height/2, width)

    def renderShader(self):
        c = self.canvas
        width,height = self.pageSize
        #c.line(0,width, height/2, 0)
        #c.line(0+10,width, height/2-10, 0)
        #c.line(0-10,width, height/2+10, 0)

    def renderPrivKey(self):
        c = self.canvas
        x,y,w,h = self.privRectangle
        q = self.createQrCode(self.privk)
        c.setFillColor(black)
        c.drawCentredString(x+w/2,y+h*.8,self.privk)

        # seed string
        if self.seed:
            self.drawSeedWrapped(self.seed)

        c.setFillColor(red)
        
        c.drawCentredString(x+w/2.,y+h*.5,"Private Key")

        bounds = q.getBounds()
        width = bounds[2] - bounds[0]
        height = bounds[3] - bounds[1]
        d = Drawing(0, 0)
        d.add(q)
        renderPDF.draw(d, c, x+(w*3/4)-width/2, y+5)

    def renderPubKey(self):
        c = self.canvas
        x,y,w,h = self.pubRectangle
        q = self.createQrCode(self.pubk)
        c.setFillColor(black)
        c.drawCentredString(x+w/2,y+h*.9,self.pubk)

        c.setFillColor(green)
        c.drawCentredString(x+w/2.,y+h*.5, f"{self.coin} Wallet")
        c.setFillColor(black)

        c.drawCentredString(x+w/2.,y+h*.01,"generated " + time.strftime("%Y-%m-%d, %H:%M:%S", time.localtime()))

        bounds = q.getBounds()
        width = bounds[2] - bounds[0]
        height = bounds[3] - bounds[1]
        d = Drawing(0, 0)
        d.add(q)
        renderPDF.draw(d, c, x+(w*1/4)-width/2, y+10)

        # masking the location of priv key qr
        #c.setFillColor(red)
        #c.rect(x+(w*3/4)-width/2,y+10, width, height, fill=True, stroke=False)
        logo_file = os.path.join(self.dataDir , self.coin.lower() + ".png")
        if os.path.exists(logo_file):
            c.drawImage( logo_file,
                        x+(w*3/4)-width/2, y-h*.7,
                        width=width,
                        preserveAspectRatio=True,
                        mask='auto')

    def createQrCode(self, str):
        qr_code = qr.QrCodeWidget(str)
        bounds = qr_code.getBounds()
        width = bounds[2] - bounds[0]
        height = bounds[3] - bounds[1]
        img_width = 90.
        d = Drawing(img_width, img_width, transform=[img_width/width,0,0,img_width/height,0,0])
        d.add(qr_code)
        return d

    def drawSeedWrapped(self, seed):
        c = self.canvas
        x,y,w,h = self.privRectangle
        xpos = x
        ypos = y+h*.3
        to = c.beginText(xpos,ypos)
        lines = textwrap.wrap(seed, 42)
        for l in lines:
            to.textLine(l)
        c.drawText(to)
