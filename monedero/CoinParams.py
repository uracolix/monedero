import os
params  = {
"BLK": {
        "config" : os.path.expanduser("~/.blackcoin/blackcoin.conf")
    }    
}

def getRpcCredentialsFromConfig():
    with open(params['BLK']['config'], "r") as c:
        val = {}
        for l in c.readlines():
            try:
                k,v = l[:l.find("#")].split("=")
                val[k] = v
            except Exception as e:
                pass
                print( "+++",l, e )
            print( l )
        c.close()
    print( val )
    return val.get("rpcuser", ""), val.get("rpcpassword", "")

if __name__ == "__main__":
    print( getRpcCredentialsFromConfig() )