import cryptos
from cryptos import sha256
from cryptos.coins_async import BaseCoin, Bitcoin, BitcoinCash, Dash, Litecoin, Doge
import getpass

class Wallet:
    CoinList = [Bitcoin, Litecoin, BitcoinCash, Dash, Doge]
    AvailableCoins = {coin.coin_symbol: coin for coin in CoinList}

    def __init__(self):
        self.CurrentCoin = None
        self.SeedPhrase = None
        self.PrivatKey = None
        self.PubAddr = None
        pass

    def list_coins(self):
        return (cs.coin_symbol for cs in self.CoinList)

    def set_coin(self, coin_symbol: str, testnet: bool = False) -> BaseCoin:
        symbol = coin_symbol.upper()
        self.CurrentCoin = self.AvailableCoins.get(symbol, None)

    def set_seed(self, seed_phrase: str):
        self.SeedPhrase = seed_phrase
        self.PrivatKey = sha256(self.SeedPhrase)

    def get_addr(self):
        if (self.PrivatKey or self.SeedPhrase) and self.CurrentCoin:
            if not self.PrivatKey:
                self.PrivatKey = sha256(self.SeedPhrase)
            self.PubAddr = self.CurrentCoin().privtoaddr(self.PrivatKey)
        return self.PubAddr
    
    def dump(self, secrets=False):
        if self.CurrentCoin:
            print(f"coin: {self.CurrentCoin.display_name} {self.CurrentCoin.coin_symbol}")
        else:
            print("coin: undefined")
        print(f"addr: {self.get_addr()}")
        if secrets:
            if self.SeedPhrase:
                print(f"seed: {self.SeedPhrase}")
            else:
                print("seed: undefined")
            if self.PrivatKey:
                print(f"priv: {self.PrivatKey}")
            else:
                print("priv: undefined")
        
    def query_missing_stuff(self):
        if self.CurrentCoin == None:
            print(f"enter coin {sorted(list(self.list_coins()))}: ", end="")
            coin_type = input()
            self.set_coin(coin_type.lower())
        if self.PrivatKey == None and self.SeedPhrase == None:
            print("enter priv key option [1 = enter seed phrase, 2 = enter key]:", end = "")
            pk_opt = int(input())
            if pk_opt == 1:
                print("enter seed phrase:", end="")
                sp = input()
                self.set_seed(sp.strip())
            elif pk_opt == 2:
                p1 = getpass.getpass("enter private key:").rstrip()
                if len(p1):
                    p2 = getpass.getpass("confirm private key:").rstrip()
                    if p1 == p2:
                        print("set key, len", len(p1))
                        self.PrivatKey = p1
                        self.SeedPhrase = None
            