import requests
import json
import time 
import getpass
import monedero.CoinParams as CoinParams

class RPCHost(object):
    def __init__(self, url = None):
        self._session = requests.Session()
        self._url = url
        self._headers = {'content-type': 'application/json'}
        
    def setServerUrl(self, rpcHost = "localhost", rpcPort = 1234, rpcUser = "", rpcPassword = ""):
        url = serverURL = 'http://' + rpcUser + ':' + rpcPassword + '@localhost:' + str(rpcPort)
        self._url = url 
        return url
        
    def call(self, rpcMethod, *params):
        payload = json.dumps({"method": rpcMethod, "params": list(params), "jsonrpc": "2.0"})
        tries = 10
        hadConnectionFailures = False
        while True:
            try:
                response = self._session.get(self._url, headers=self._headers, data=payload)
            except requests.exceptions.ConnectionError:
                tries -= 1
                if tries == 0:
                    raise Exception('Failed to connect for remote procedure call.')
                hadFailedConnections = True
                print("Couldn't connect for remote procedure call, will sleep for ten seconds and then try again ({} more tries)".format(tries))
                time.sleep(10)
            else:
                if hadConnectionFailures:
                    print('Connected for remote procedure call after retry.')
                break
        if not response.status_code in (200, 500):
            raise Exception('RPC connection failure: ' + str(response.status_code) + ' ' + response.reason)
        responseJSON = response.json()
        if 'error' in responseJSON and responseJSON['error'] != None:
            raise Exception('Error in RPC call: ' + str(responseJSON['error']))
        return responseJSON['result']

def getKey(account):    
    rpcHost = "localhost"
    rpcPort = 8332
    rpcUser, rpcPassword = CoinParams.getRpcCredentialsFromConfig()
    rh = RPCHost()
    rh.setServerUrl(rpcHost, rpcPort, rpcUser, rpcPassword)
    pubaddr = rh.call("getaddressesbyaccount", account)[0]
    pw = getpass.getpass("Walletpassphrase:")
    rh.call("walletpassphrase", pw, 10)
    privk = rh.call("dumpprivkey", pubaddr)
    rh.call("walletlock")
    return privk, pubaddr
    

if __name__ == "__main__":
    import getpass
    import pprint

    rpcHost = "localhost"
    rpcPort = 8332
    rpcUser = 'blackcoinrpc'
    rpcPassword = ''
    rh = RPCHost()
    rh.setServerUrl(rpcHost, rpcPort, rpcUser, rpcPassword)
    account = "XYZ123"
    pprint.pprint(rh.call("getinfo"))
    addr = rh.call("getaddressesbyaccount", "XYZ123")[0]
    pw = getpass.getpass("Walletpassphrase:")
    try:
        
        pprint.pprint(rh.call("walletpassphrase", pw, 10))
    except Exception as e:
        print( e )
        
    print( "Account:", account, rh.call("getaccount", addr))
    print( "PrivKey:", rh.call("dumpprivkey", addr))
    rh.call("walletlock")
    print( "PubAddr:", addr )
    
    print( "multiple addresses per account label:", rh.call("getaddressesbyaccount", "zz_Axel") )
    print( "list accounts:",  rh.call("listaccounts") )