#!/usr/bin/env python2
# Paper Wallet for BlackCoin

"""

    %prog [options]

%prog - A Python Paper Wallet Generator
==============================================

%prog generates a (encrypted) PDF file that displays the private key and 
public *Coin address.

For the key generation the script http://we.lovebitco.in/paperwal.py is employed.

---------------------------------------------------------------------------------
The author of mondero takes no responsibility for the cryptographic correctnes
of the generated keys.
---------------------------------------------------------------------------------
"""

epilog = """
Examples:
./monedero.py
    
    Query for a PDF passphrase and requests entropy key presses
    and generates wallet.pdf
    
./monedero.py -o my_wallet.pdf 

    Write the file my_wallet.pdf
    
./monedero.py -P mysupersecurepasswd

    For Batch Generation of several paper wallets, you can provide 
    the PDF passphrase by command line. !Attention! the passphrase 
    might be stored in the command line history.

See also http://monedero.readthedocs.io for full documentation.

"""
import monedero.KeyGenerator2 as KeyGenerator
from monedero.Wallet import Wallet as Wallet
import monedero.PdfWallet
import monedero.RpcHost
from  monedero import __version__


from optparse import OptionParser, OptionGroup
import getpass
from reportlab.lib.pagesizes import A4

class MyOptionParser(OptionParser):
    def format_epilog(self, formatter):
        return self.epilog

def getOptions():

    parser = MyOptionParser( usage = __doc__,
                           version = "%%prog %s" % __version__,
                           epilog = epilog)
    
    parser.add_option("-c", "--coin", dest="coin_type", default=None,
                      help="coin type of the paper wallet")
    parser.add_option("-s", "--secret", action='store_true', dest="secret", default=False,
                      help="show wallets secrets")
    parser.add_option("-p", "--nopause", action='store_true', dest="nopause", default=True,
                      help="disable the pause before exiting")
    parser.add_option("-o", "--output", dest='pdffilename', default='wallet.pdf',
                      help="name of the generated PDF file (default: wallet.pdf)")
    parser.add_option("-P", "--password", dest='pdfpasswd', default=None,
                      help="protect PDF file with password")
    # parser.add_option("-e", "--encrypted", action='store_true', dest="encrypted", default=False,
    #                   help="create BIP38-encrypted privkey (takes a LONG time)")
    # parser.add_option("-v", "--validate", action='store_true', dest="validate", default=False,
    #                   help="enable extensive system tests for entropy")
    # parser.add_option("-l", "--loop", action='store_true', dest="repeat", default=False,
    #                   help="restart instead of exit")
    # parser.add_option("-d", "--doublecalc", action='store_true', dest="doublecalc", default=False,
    #                   help="calculate twice and test result")
    # parser.add_option("-z", "", dest='just a helpful hint', default='',
    #                   help="try ctrl-tab to abort the program")
    # parser.add_option("-w", "--wallet", dest='wallet_account', default=None,
    #                   help="make paper wallet from wallet account")
    entropy_warning = OptionGroup(parser, "Warning",
                                          "If you use this option, you should supply REAL randomly generated entropy. "
                                          "It is probably a good idea not to reuse a seed.")
    entropy_warning.add_option("-r", "--entropy", dest='entropy', default='',
                               help="random seed instead of keypresses, 64+ characters")
    parser.add_option_group(entropy_warning)
    (options, args) = parser.parse_args()
    return (options, args)

def validateOptions(options):
    if options.pdfpasswd == None:
        p1 = getpass.getpass("enter pdf password:").rstrip()
        if len(p1):
            p2 = getpass.getpass("confirm pdf password:").rstrip()
            if p1 == p2:
                options.pdfpasswd = p1
            else:
                raise Exception("password comfirmation failed")
    if (not options.pdfpasswd) or (len(options.pdfpasswd) < 1):
        print( "PdfFile will not be encrypted!" )
        options.pdfpasswd = None

if __name__ == "__main__":
    (options, args) = getOptions()
    validateOptions(options)

    #if options.wallet_account != None:
    #    privk, pubk = monedero.RpcHost.getKey(options.wallet_account)
    #else:
    #    privk, pubk = KeyGenerator.paperwal(options, args)
    ww = Wallet()
    if options.coin_type:
        ww.set_coin(options.coin_type, False)
    ww.query_missing_stuff()
    ww.dump(options.secret)
    w = monedero.PdfWallet.PdfWallet(ww.PrivatKey, ww.PubAddr,
                                coin = ww.CurrentCoin.coin_symbol,
                                seed = ww.SeedPhrase,
                                fileName = options.pdffilename,
                                encryptPdf = options.pdfpasswd,
                                pageSize=A4)
    w.render()
    w.save()
    print( "Wrote file: ", options.pdffilename )
    
